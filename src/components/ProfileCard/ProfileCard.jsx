import { useNavigate } from "react-router-dom";
import { deleteUser } from "../../Services/RESTService";
import { deleteCookie } from "../../Services/RESTService";
import { cookieTypes } from "../../Services/config/rest-service-config";
import { useContext, useId } from "react";
import { AuthContext } from "../../contexts/AuthContext";

export default function ProfileCard({ profileData, setCurrentUser }) {
  const navigate = useNavigate();

  const randId = useId()

  const handleDeleteClick = async () => {
    const response = await deleteUser(email);
    if (response == 200) {
      setCurrentUser({ nome: "", cognome: "", email: "", ruoli: [] });
      deleteCookie(cookieTypes.jwt);
      navigate("/");
    }
  };

  //prendo i dati da stampare nella card dal contesto
  const {currentUser} = useContext(AuthContext)

  return (
    <div className="card" style={{ width: "18rem" }}>
      <img src="..." className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">
          {currentUser.nome} {currentUser.cognome}
        </h5>
        <h5 className="card-title">{currentUser.email}</h5>
        {currentUser?.ruoli?.map((ruolo) => (
          <h5 key={randId} className="card-title">{ruolo.tipologia}</h5>
        ))}
        {/*<button className="btn btn-danger" onClick={handleDeleteClick}>
          Elimina
        </button>*/}
      </div>
    </div>
  );
}
