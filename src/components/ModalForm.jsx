import Button from "./Button/Button";
import { useState } from "react";

export default function ModalForm({ aggiungiCorso }) {
  const [nuovoCorso, setNuovoCorso] = useState({
    nome_corso: "",
    categoria : {id_ca: "", nome_categoria:""},
    durata: "",
    descrizione_breve: "",
    descrizione_completa: "",
    id_doc:"7"
  });

  const handleChange = (e) => {
    const { name, value } = e.target;

    if(name === "id_ca" || name === "nome_categoria") {
      console.log({[name]: value})
      setNuovoCorso({...nuovoCorso, categoria:{ ...nuovoCorso, [name]: value }})

      
    }
    setNuovoCorso({ ...nuovoCorso, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    aggiungiCorso(nuovoCorso);
    setNuovoCorso({
      nome_corso: "",
      categoria : {id_ca: "", nome_categoria:""},
      durata: "",
      descrizione_breve: "",
      descrizione_completa: "",
      id_doc:"7"
    });
    console.log(JSON.stringify(nuovoCorso))
  };

  return (
    <>
      <Button
        type="button"
        className="btn btn-success ms-3 mt-3 addCourseButton"
        data-bs-toggle="modal"
        data-bs-target="#addNewCourseModal"
      >
        Aggiungi corso
      </Button>

      <div
        className="modal fade"
        id="addNewCourseModal"
        tabIndex="-1"
        aria-labelledby="aggiungiNuovoCorsoModale"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="aggiungiNuovoCorsoModale">
                Nuovo corso
              </h1>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form id="formNuovoCorso" onSubmit={handleSubmit}>
                <div className="mb-3">
                  <input
                    required
                    name="nome_corso"
                    type="text"
                    className="form-control"
                    id="nome_corso"
                    placeholder="Nome corso"
                    value={nuovoCorso.nome_corso}
                    onChange={handleChange}
                  />
                </div>
                <div className="mb-3">
                  <label>
                    Durata
                    <select
                      name="durata"
                      form="formNuovoCorso"
                      required
                      value={nuovoCorso.durata}
                      onChange={handleChange}
                    >
                      <option value="">Seleziona una durata</option>
                      <option value="8">8 ore</option>
                      <option value="40">40 ore</option>
                      <option value="160">160 ore</option>
                      <option value="480">480 ore</option>
                      <option value="960">960 ore</option>
                    </select>
                  </label>
                  <div className="mb-3">
                    <label>
                      Categoria
                      <select
                        name="id_ca"
                        form="formNuovoCorso"
                        required
                        value={nuovoCorso.id_ca}
                        onChange={handleChange}
                      >
                        <option value="">Seleziona una categoria</option>
                        <option value="1">FrontEnd</option>
                        <option value="2">BackEnd</option>
                      </select>
                    </label>
                  </div>
                </div>
                <div className="mb-3">
                  <input
                    required
                    name="descrizione_breve"
                    className="form-control"
                    id="descrizione_breve"
                    placeholder="Descrizione breve"
                    value={nuovoCorso.descrizione_breve}
                    onChange={handleChange}
                  />
                </div>
                <div className="mb-3">
                  <textarea
                    required
                    name="descrizione_completa"
                    className="form-control"
                    id="descrizione_completa"
                    placeholder="Descrizione completa"
                    value={nuovoCorso.descrizione_completa}
                    onChange={handleChange}
                  />
                </div>
                <div className="d-flex flex-column ">
                  <button
                    type="button"
                    className="btn btn-warning mb-1"
                    data-bs-dismiss="modal"
                  >
                    Annulla
                  </button>
                  <button
                    type="submit"
                    data-bs-dismiss="modal"
                    className="btn btn-success mt-1"
                  >
                    Aggiungi
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
