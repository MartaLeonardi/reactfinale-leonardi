import css from "./CourseCard.module.css";
import Button from "../Button/Button";
import { useNavigate } from "react-router-dom";
import { deleteCourse } from "../../Services/RESTService";
import { useEffect, useState } from "react";

export default function CourseCard({
  immagineCopertina,
  id_c,
  nome_corso,
  durata,
  descrizione_breve,
  currentUser,
  id_ca,
  fetchData,
}) {

  const navigateTo = useNavigate();

  const [nome_categoria, setNomeCategoria] = useState("")

  useEffect(() => {
    convertiIdCategoriaInNomeCategoria(id_ca)
    console.log(id_ca)
  }, [id_ca])

  const handleClick = () => {
    navigateTo(`/corsi/${nome_corso}`);
  };

  const handleDeleteClick = async () => {
    const response = await deleteCourse(id_c);
    if(response == 200){
      fetchData();
    }
  };

  const convertiIdCategoriaInNomeCategoria = (id_ca) => {
    if(id_ca == "1"){
      setNomeCategoria("FrontEnd")
      console.log(nome_categoria)
    }else if(id_ca == "2"){
      setNomeCategoria("BackEnd")
      console.log(nome_categoria)
    }
  }


  return (
    <div className="d-flex flex-column">
      <div
        className={`card mt-5 shadow ${css.myCourseCard}`}
        onClick={handleClick}
      >
        <img src={immagineCopertina} className="card-img-top" alt="..." />
        <div className="card-body">
          <img src="" alt="" />
          <h4 className="card-title">{nome_corso}</h4>
          
          <h6 className="card-title">{durata} ore</h6>
          <p className="card-text">{descrizione_breve}</p>
        </div>
      </div>
      {currentUser.ruoli.includes("Admin") ? (
        <Button className="btn btn-danger mt-2" onClick={handleDeleteClick}>
          Elimina
        </Button>
      ) : (
        <></>
      )}
    </div>
  );
}
